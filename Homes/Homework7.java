public class Homework7 {
    /*
На вход подается последовательность чисел, оканчивающихся на -1.
Необходимо вывести число, которое присутствует в последовательности минимальное количество раз.

Гарантируется: 

Все числа в диапазоне от -100 до  100.

Числа встречаются не более  2 147 483 647 раз каждое.

Сложность алгоритма - O(n)

*/




    public static void main(String[] args) {
        // создаём HashMap ключом будет число, а значением - количество повторений
        HashMap<Integer, Integer> arrayAduser = new HasHMap<>();
        // выводим на печать сообщение
        System.out.println ("Введите последовательность чисел от -100 до 100. Для прекращения ввода введите -1");
        // объявляю переменную для сканирования ввода с консоли
        Scanner scanner = new Scanner(System.in);
        // объявляю переменную для хранения числа последовательности
        int inNumber - scanner.nextInt();
        // пока число не равно -1 и если число в диапазоне от -100 до  100 добовляем число в последователньость 
        while (inNumber != -1) {
            if (inNumber > -101 && inNumber < 101) {
                // если число уже есть в последовательности, то прибавляем к количеству повторений 1
                // иначе записываем в значении 1
                if (arrayAduser.containsKey(inNumber)) {
                    int count = array Aduser.get(InNumber);
                    arrayAduser.put(inNumber, count + 1);
                } else {
                    arrayAduser.put(inNumber, 1);
                }
            }
            // берём следующее число
            inNumber - scanner.nextInt();
        }
        // переменная для хранения минимального значения в HashMap
        int minValueInMap = (Collections.min(arrayAduser.values()));
        // Перебираем Hachmap для поиска ключа с наименьшим значением
        for (Map.Entry<Integer, Integer> entry : arrayAduser.entrySet()) {
            // если нашли, то выводим на печатҗ
            if (entry.getValue() == minValueInMap) {
                System.out.println(entry.getKey());
            }
        }
    }
}

