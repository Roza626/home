public class Human implements Comparable<Human> {

    private String name;
    private double weight;

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight() {
        this.weight = randomWeight();
    }

    public double randomWeight() {
        int minWeight = 40;
        int maxWeight = 140;
        int max = maxWeight - minWeight;
        return (Math.random() * max) + minWeight;
    }

    public int compareTo(Human o) {
        return (int) (this.weight - o.weight);
    }
}





public class main {
    public static void main(String[] args) {
        Human[] humans = new Human[10];
        humans[0] = new Human ("Roza" 100);
        humans[1] = new Human ("Masa", 71);
        humans[2] = new Human ("Sonja", 50.1);
        humans[3] = new Human ("Any", 90);
        humans[4] = new Human ("R", 120);
        humans[5] = new Human ("Elena", 50);
        humans[6] = new Human ("Silvi", 60);
        humans[7] = new Human ("Fifa", 45);
        humans[8] = new Human ("Sofa", 75);
        humans[9] = new Human ("Marfa", 110);

        for (int i =0; i < humans.length; i++) {
            double min humans[i].getWeight();
            int minIndex = i;
            for (int j = i + 1; j < humans.length; j++) {
                if (humans[j].getWeight() < min) {
                    min = humans [j].getWeight();
                    minIndex = j;
                    }
                }
                Human temp = humans[i];
                humans[i] = humans[minIndex];
                humans[minIndex] = temp;
                }
            for (int i = 0; i<humans.lenght;i++) {
            System.out.println(humans[i].name + " " + humans[i].getWeight());
            }
    }
}
